#!/bin/bash

check_command "rsync"

function jira_perform_rsync {
    RSYNC_QUIET=-q
    if [ "${JIRA_VERBOSE_BACKUP}" == "TRUE" ]; then
        RSYNC_QUIET=
    fi

    mkdir -p ${JIRA_BACKUP_HOME}
    rsync -avh ${RSYNC_QUIET} --delete --delete-excluded --exclude=/export/ --exclude=/import/ --exclude=/tmp/ --exclude=/*.lock ${JIRA_HOME} ${JIRA_BACKUP_HOME}
    if [ $? != 0 ]; then
        bail "Unable to rsync from ${JIRA_HOME} to ${JIRA_BACKUP_HOME}"
    fi
}

function jira_prepare_home {
    info "Prepared backup of ${JIRA_HOME} to ${JIRA_BACKUP_HOME}"
}

function jira_backup_home {
    jira_perform_rsync
    info "Performed backup of ${JIRA_HOME} to ${JIRA_BACKUP_HOME}"
}

function jira_restore_home {
    RSYNC_QUIET=-q
    if [ "${JIRA_VERBOSE_BACKUP}" == "TRUE" ]; then
        RSYNC_QUIET=
    fi

    rsync -av ${RSYNC_QUIET} ${JIRA_RESTORE_HOME}/ ${JIRA_HOME}/
    info "Performed restore of ${JIRA_RESTORE_HOME} to ${JIRA_HOME}"
}
