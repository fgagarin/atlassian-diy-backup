#!/bin/bash

check_command "tar"

function jira_backup_archive {
    mkdir -p ${JIRA_BACKUP_ARCHIVE_ROOT}
    JIRA_BACKUP_ARCHIVE_NAME=`perl -we 'use Time::Piece; my $sydTime = localtime; print "jira-", $sydTime->strftime("%Y%m%d-%H%M%S-"), substr($sydTime->epoch, -3), ".tar.gz"'`

    info "Archiving ${JIRA_BACKUP_ROOT} into ${JIRA_BACKUP_ARCHIVE_ROOT}/${JIRA_BACKUP_ARCHIVE_NAME}"
    tar -czf ${JIRA_BACKUP_ARCHIVE_ROOT}/${JIRA_BACKUP_ARCHIVE_NAME} -C ${JIRA_BACKUP_ROOT} .
    info "Archived ${JIRA_BACKUP_ROOT} into ${JIRA_BACKUP_ARCHIVE_ROOT}/${JIRA_BACKUP_ARCHIVE_NAME}"
}

function jira_restore_archive {
    if [ -f ${JIRA_BACKUP_ARCHIVE_NAME} ]; then
        JIRA_BACKUP_ARCHIVE_NAME=${JIRA_BACKUP_ARCHIVE_NAME}
    else
        JIRA_BACKUP_ARCHIVE_NAME=${JIRA_BACKUP_ARCHIVE_ROOT}/${JIRA_BACKUP_ARCHIVE_NAME}
    fi
    tar -xzf ${JIRA_BACKUP_ARCHIVE_NAME} -C ${JIRA_RESTORE_ROOT}

    info "Extracted ${JIRA_BACKUP_ARCHIVE_NAME} into ${JIRA_RESTORE_ROOT}"
}
