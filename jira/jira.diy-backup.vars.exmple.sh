#!/bin/bash

##
# It is recomended to `chmod 600 jira.diy-backup.vars.sh` after copying the template.
##

#CURL_OPTIONS="-L -s -f"

# Which database backup script to use (ex: mssql, postgresql, mysql, ebs-collocated, rds)
BACKUP_DATABASE_TYPE=postgresql

# Which filesystem backup script to use (ex: rsync, ebs-home)
BACKUP_HOME_TYPE=rsync

# Which archive backup script to use (ex: tar, tar-gpg)
BACKUP_ARCHIVE_TYPE=tar

# Used by the scripts for verbose logging. If not true only errors will be shown.
JIRA_VERBOSE_BACKUP=TRUE

# The base url used to access this stash instance. It cannot end on a '/'
#JIRA_URL=localhost:7990

# Used in AWS backup / restore to tag snapshots. It cannot contain spaces and it must be under 100 characters long
INSTANCE_NAME=jira

# The username and password for the user used to make backups (and have this permission)
#JIRA_BACKUP_USER=admin
#JIRA_BACKUP_PASS=admin

# The name of the database used by this instance.
JIRA_DB=jira
# The path to stash home folder (with trailing /)
JIRA_HOME=/var/atlassian/jira/


# OS level user and group information (typically: 'atlstash' for both)
JIRA_UID=daemon
JIRA_GID=daemon

# The path to working folder for the backup
JIRA_BACKUP_ROOT=/var/backups/jira/tmp
# DO NOT CHANGE FOLLOWING FOLDER!!! (they affect to restore script)
JIRA_BACKUP_DB=${JIRA_BACKUP_ROOT}/db/
JIRA_BACKUP_HOME=${JIRA_BACKUP_ROOT}/home/

# The path to where the backup archives are stored
JIRA_BACKUP_ARCHIVE_ROOT=/var/backups/jira

# List of repo IDs which should be excluded from the backup
# It should be space separated: (2 5 88)
#JIRA_BACKUP_EXCLUDE_REPOS=()

# PostgreSQL options
#POSTGRES_HOST=$(docker inspect postgres | sed -n 's/^.*"\(IPAddress\).*"\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)".*/\2/p')
POSTGRES_HOST=localhost
POSTGRES_USERNAME=jira
POSTGRES_PASSWORD=jira
POSTGRES_PORT=5432

# Options for the tar-gpg archive type
JIRA_BACKUP_GPG_RECIPIENT=
