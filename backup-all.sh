#!/bin/bash

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

$SCRIPTPATH/jira/jira.diy-backup.sh
$SCRIPTPATH/confluence/confluence.diy-backup.sh
$SCRIPTPATH/stash/stash.diy-backup.sh

# delete old backups (older than 10 days)
find /var/backups/ \
	-size +300M \
	-ctime +10 \
	! -path "/var/backups/hdd/*" \
	-a ! -path "/var/backups/stash/tmp/*" \
	-type f \
	-delete
