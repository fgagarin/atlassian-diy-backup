#!/bin/bash

##
# It is recomended to `chmod 600 confluence.diy-backup.vars.sh` after copying the template.
##

#CURL_OPTIONS="-L -s -f"

# Which database backup script to use (ex: mssql, postgresql, mysql, ebs-collocated, rds)
BACKUP_DATABASE_TYPE=postgresql

# Which filesystem backup script to use (ex: rsync, ebs-home)
BACKUP_HOME_TYPE=rsync

# Which archive backup script to use (ex: tar, tar-gpg)
BACKUP_ARCHIVE_TYPE=tar

# Used by the scripts for verbose logging. If not true only errors will be shown.
CONFLUENCE_VERBOSE_BACKUP=TRUE

# The name of the database used by this instance.
CONFLUENCE_DB=confluence
# The path to stash home folder (with trailing /)
CONFLUENCE_HOME=/var/atlassian/confluence/


# OS level user and group information (typically: 'atlstash' for both)
CONFLUENCE_UID=daemon
CONFLUENCE_GID=daemon

# The path to working folder for the backup
CONFLUENCE_BACKUP_ROOT=/var/backups/confluence/tmp
# DO NOT CHANGE FOLLOWING FOLDER!!! (they affect to restore script)
CONFLUENCE_BACKUP_DB=${CONFLUENCE_BACKUP_ROOT}/db/
CONFLUENCE_BACKUP_HOME=${CONFLUENCE_BACKUP_ROOT}/home/

# The path to where the backup archives are stored
CONFLUENCE_BACKUP_ARCHIVE_ROOT=/var/backups/confluence

# PostgreSQL options
#POSTGRES_HOST=$(docker inspect postgres | sed -n 's/^.*"\(IPAddress\).*"\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)".*/\2/p')
POSTGRES_HOST=localhost
POSTGRES_USERNAME=confluence
POSTGRES_PASSWORD=confluence
POSTGRES_PORT=5432
