#!/bin/bash

check_command "curl"
check_command "jq"

CONFLUENCE_HTTP_AUTH="-u ${CONFLUENCE_BACKUP_USER}:${CONFLUENCE_BACKUP_PASS}"

# The name of the product
PRODUCT=Stash

function confluence_lock {
    CONFLUENCE_LOCK_RESULT=`curl ${CURL_OPTIONS} ${CONFLUENCE_HTTP_AUTH} -X POST -H "Content-type: application/json" "${CONFLUENCE_URL}/mvc/maintenance/lock"`
    if [ -z "${CONFLUENCE_LOCK_RESULT}" ]; then
        bail "Locking this Stash instance failed"
    fi

    CONFLUENCE_LOCK_TOKEN=`echo ${CONFLUENCE_LOCK_RESULT} | jq -r ".unlockToken"`
    if [ -z "${CONFLUENCE_LOCK_TOKEN}" ]; then
        bail "Unable to find lock token. Result was '$CONFLUENCE_LOCK_RESULT'"
    fi

    info "locked with '$CONFLUENCE_LOCK_TOKEN'"
}

function confluence_backup_start {
    CONFLUENCE_BACKUP_RESULT=`curl ${CURL_OPTIONS} ${CONFLUENCE_HTTP_AUTH} -X POST -H "X-Atlassian-Maintenance-Token: ${CONFLUENCE_LOCK_TOKEN}" -H "Accept: application/json" -H "Content-type: application/json" "${CONFLUENCE_URL}/mvc/admin/backups?external=true"`
    if [ -z "${CONFLUENCE_BACKUP_RESULT}" ]; then
        bail "Entering backup mode failed"
    fi

    CONFLUENCE_BACKUP_TOKEN=`echo ${CONFLUENCE_BACKUP_RESULT} | jq -r ".cancelToken"`
    if [ -z "${CONFLUENCE_BACKUP_TOKEN}" ]; then
        bail "Unable to find backup token. Result was '${CONFLUENCE_BACKUP_RESULT}'"
    fi

    info "backup started with '${CONFLUENCE_BACKUP_TOKEN}'"
}

function confluence_backup_wait {
    CONFLUENCE_PROGRESS_DB_STATE="AVAILABLE"
    CONFLUENCE_PROGRESS_SCM_STATE="AVAILABLE"

    print -n "[${CONFLUENCE_URL}]  INFO: Waiting for DRAINED state"
    while [ "${CONFLUENCE_PROGRESS_DB_STATE}_${CONFLUENCE_PROGRESS_SCM_STATE}" != "DRAINED_DRAINED" ]; do
        print -n "."

        CONFLUENCE_PROGRESS_RESULT=`curl ${CURL_OPTIONS} ${CONFLUENCE_HTTP_AUTH} -X GET -H "X-Atlassian-Maintenance-Token: ${CONFLUENCE_LOCK_TOKEN}" -H "Accept: application/json" -H "Content-type: application/json" "${CONFLUENCE_URL}/mvc/maintenance"`
        if [ -z "${CONFLUENCE_PROGRESS_RESULT}" ]; then
            bail "[${CONFLUENCE_URL}] ERROR: Unable to check for backup progress"
        fi

        CONFLUENCE_PROGRESS_DB_STATE=`echo ${CONFLUENCE_PROGRESS_RESULT} | jq -r '.["db-state"]'`
        CONFLUENCE_PROGRESS_SCM_STATE=`echo ${CONFLUENCE_PROGRESS_RESULT} | jq -r '.["scm-state"]'`
        CONFLUENCE_PROGRESS_STATE=`echo ${CONFLUENCE_PROGRESS_RESULT} | jq -r '.task.state'`

        if [ "${CONFLUENCE_PROGRESS_STATE}" != "RUNNING" ]; then
            error "Unable to start backup, try unlocking"
            confluence_unlock
            bail "Failed to start backup"
        fi
    done

    print " done"
    info "db state '${CONFLUENCE_PROGRESS_DB_STATE}'"
    info "scm state '${CONFLUENCE_PROGRESS_SCM_STATE}'"
}

function confluence_backup_progress {
    CONFLUENCE_REPORT_RESULT=`curl ${CURL_OPTIONS} ${CONFLUENCE_HTTP_AUTH} -X POST -H "Accept: application/json" -H "Content-type: application/json" "${CONFLUENCE_URL}/mvc/admin/backups/progress/client?token=${CONFLUENCE_LOCK_TOKEN}&percentage=$1"`
    if [ $? != 0 ]; then
        bail "Unable to update backup progress"
    fi

    info "Backup progress updated to $1"
}

function confluence_unlock {
    CONFLUENCE_UNLOCK_RESULT=`curl ${CURL_OPTIONS} ${CONFLUENCE_HTTP_AUTH} -X DELETE -H "Accept: application/json" -H "Content-type: application/json" "${CONFLUENCE_URL}/mvc/maintenance/lock?token=${CONFLUENCE_LOCK_TOKEN}"`
    if [ $? != 0 ]; then
        bail "Unable to unlock instance with lock ${CONFLUENCE_LOCK_TOKEN}"
    fi

    info "Stash instance unlocked"
}

function freeze_mount_point {
    info "Freezing filesystem at mount point ${1}"

    sudo fsfreeze -f ${1} > /dev/null
}

function unfreeze_mount_point {
    info "Unfreezing filesystem at mount point ${1}"

    sudo fsfreeze -u ${1} > /dev/null 2>&1
}

function mount_device {
    local DEVICE_NAME="$1"
    local MOUNT_POINT="$2"

    sudo mount "${DEVICE_NAME}" "${MOUNT_POINT}" > /dev/null
    success "Mounted device ${DEVICE_NAME} to ${MOUNT_POINT}"
}

function add_cleanup_routine() {
    cleanup_queue+=($1)
    trap run_cleanup EXIT
}

function run_cleanup() {
    info "Cleaning up..."
    for cleanup in ${cleanup_queue[@]}
    do
        ${cleanup}
    done
}

function check_mount_point {
    local MOUNT_POINT="${1}"

    # mountpoint check will return a non-zero exit code when mount point is free
    mountpoint -q "${MOUNT_POINT}"
    if [ $? == 0 ]; then
        error "The directory mount point ${MOUNT_POINT} appears to be taken"
        bail "Please stop Stash. Stop PostgreSQL if it is running. Unmount the device and detach the volume"
    fi
}

# This removes config.lock, index.lock, gc.pid, and refs/heads/*.lock
function cleanup_locks {
    local HOME_DIRECTORY="$1"

    # From the shopt man page:
    # globstar
    #           If set, the pattern ‘**’ used in a filename expansion context will match all files and zero or
    #           more directories and subdirectories. If the pattern is followed by a ‘/’, only directories and subdirectories match.
    shopt -s globstar

    # Remove lock files in the repositories
    sudo -u ${CONFLUENCE_UID} rm -f ${HOME_DIRECTORY}/shared/data/repositories/*/{HEAD,config,index,gc,packed-refs,stash-packed-refs}.{pid,lock}
    sudo -u ${CONFLUENCE_UID} rm -f ${HOME_DIRECTORY}/shared/data/repositories/*/refs/**/*.lock
    sudo -u ${CONFLUENCE_UID} rm -f ${HOME_DIRECTORY}/shared/data/repositories/*/stash-refs/**/*.lock
    sudo -u ${CONFLUENCE_UID} rm -f ${HOME_DIRECTORY}/shared/data/repositories/*/logs/**/*.lock
}