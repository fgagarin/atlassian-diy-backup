#!/bin/bash

check_command "tar"

function confluence_backup_archive {
    mkdir -p ${CONFLUENCE_BACKUP_ARCHIVE_ROOT}
    CONFLUENCE_BACKUP_ARCHIVE_NAME=`perl -we 'use Time::Piece; my $sydTime = localtime; print "confluence-", $sydTime->strftime("%Y%m%d-%H%M%S-"), substr($sydTime->epoch, -3), ".tar.gz"'`

    info "Archiving ${CONFLUENCE_BACKUP_ROOT} into ${CONFLUENCE_BACKUP_ARCHIVE_ROOT}/${CONFLUENCE_BACKUP_ARCHIVE_NAME}"
    tar -czf ${CONFLUENCE_BACKUP_ARCHIVE_ROOT}/${CONFLUENCE_BACKUP_ARCHIVE_NAME} -C ${CONFLUENCE_BACKUP_ROOT} .
    info "Archived ${CONFLUENCE_BACKUP_ROOT} into ${CONFLUENCE_BACKUP_ARCHIVE_ROOT}/${CONFLUENCE_BACKUP_ARCHIVE_NAME}"
}

function confluence_restore_archive {
    if [ -f ${CONFLUENCE_BACKUP_ARCHIVE_NAME} ]; then
        CONFLUENCE_BACKUP_ARCHIVE_NAME=${CONFLUENCE_BACKUP_ARCHIVE_NAME}
    else
        CONFLUENCE_BACKUP_ARCHIVE_NAME=${CONFLUENCE_BACKUP_ARCHIVE_ROOT}/${CONFLUENCE_BACKUP_ARCHIVE_NAME}
    fi
    tar -xzf ${CONFLUENCE_BACKUP_ARCHIVE_NAME} -C ${CONFLUENCE_RESTORE_ROOT}

    info "Extracted ${CONFLUENCE_BACKUP_ARCHIVE_NAME} into ${CONFLUENCE_RESTORE_ROOT}"
}
