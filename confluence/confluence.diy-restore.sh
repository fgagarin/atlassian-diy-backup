#!/bin/bash

SCRIPT_DIR=$(dirname $0)

# Contains util functions (bail, info, print)
source ${SCRIPT_DIR}/confluence.diy-backup.utils.sh

# BACKUP_VARS_FILE - allows override for confluence.diy-backup.vars.sh
if [ -z "${BACKUP_VARS_FILE}" ]; then
    BACKUP_VARS_FILE=${SCRIPT_DIR}/confluence.diy-backup.vars.sh
fi

# Declares other scripts which provide required backup/archive functionality
# Contains all variables used by the other scripts
if [[ -f ${BACKUP_VARS_FILE} ]]; then
    source ${BACKUP_VARS_FILE}
else
    error "${BACKUP_VARS_FILE} not found"
    bail "You should create it using ${SCRIPT_DIR}/confluence.diy-backup.vars.sh.example as a template."
fi

# Ensure we know which user:group things should be owned as
if [[ -z ${CONFLUENCE_UID} || -z ${CONFLUENCE_GID} ]]; then
  error "Both CONFLUENCE_UID and CONFLUENCE_GID must be set in confluence.diy-backup.vars.sh"
  bail "See confluence.diy-backup.vars.sh.example for the defaults."
fi

# The following scripts contain functions which are dependent on the configuration of this stash instance.
# Generally each of them exports certain functions, which can be implemented in different ways

if [ "mssql" = "${BACKUP_DATABASE_TYPE}" ] || [ "postgresql" = "${BACKUP_DATABASE_TYPE}" ] || [ "mysql" = "${BACKUP_DATABASE_TYPE}" ]; then
    # Exports the following functions
    #     confluence_restore_db     - for restoring the stash DB
    source ${SCRIPT_DIR}/confluence.diy-backup.${BACKUP_DATABASE_TYPE}.sh
else
    error "${BACKUP_DATABASE_TYPE} is not a supported database backup type"
    bail "Please update BACKUP_DATABASE_TYPE in ${BACKUP_VARS_FILE} or consider running confluence.diy-aws-restore.sh instead"
fi

if [ "rsync" = "${BACKUP_HOME_TYPE}" ]; then
    # Exports the following functions
    #     confluence_restore_home   -  for restoring the filesystem backup
    source ${SCRIPT_DIR}/confluence.diy-backup.${BACKUP_HOME_TYPE}.sh
else
    error "${BACKUP_HOME_TYPE} is not a supported home backup type"
    bail "Please update BACKUP_HOME_TYPE in ${BACKUP_VARS_FILE} or consider running confluence.diy-aws-restore.sh instead"
fi

# Exports the following functions
#     confluence_restore_archive - for un-archiving the archive folder
source ${SCRIPT_DIR}/confluence.diy-backup.${BACKUP_ARCHIVE_TYPE}.sh

##########################################################
# The actual restore process. It has the following steps

function available_backups {
	echo "Available backups:"
	ls ${CONFLUENCE_BACKUP_ARCHIVE_ROOT}
}

if [ $# -lt 1 ]; then
    echo "Usage: $0 <backup-file-name>.tar.gz"
    if [ ! -d ${CONFLUENCE_BACKUP_ARCHIVE_ROOT} ]; then
        error "${CONFLUENCE_BACKUP_ARCHIVE_ROOT} does not exist!"
    else
        available_backups
    fi
    exit 99
fi
CONFLUENCE_BACKUP_ARCHIVE_NAME=$1
if [ ! -f ${CONFLUENCE_BACKUP_ARCHIVE_ROOT}/${CONFLUENCE_BACKUP_ARCHIVE_NAME} ]; then
	error "${CONFLUENCE_BACKUP_ARCHIVE_ROOT}/${CONFLUENCE_BACKUP_ARCHIVE_NAME} does not exist!"
	available_backups
	exit 99
fi

confluence_bail_if_db_exists

# Check and create CONFLUENCE_HOME
if [ -e ${CONFLUENCE_HOME} ]; then
	bail "Cannot restore over existing contents of ${CONFLUENCE_HOME}. Please rename or delete this first."
fi
mkdir -p ${CONFLUENCE_HOME}
chown ${CONFLUENCE_UID}:${CONFLUENCE_GID} ${CONFLUENCE_HOME}

# Setup restore paths
CONFLUENCE_RESTORE_ROOT=`mktemp -d /tmp/confluence.diy-restore.XXXXXX`
CONFLUENCE_RESTORE_DB=${CONFLUENCE_RESTORE_ROOT}/db
CONFLUENCE_RESTORE_HOME=${CONFLUENCE_RESTORE_ROOT}/home

# Extract the archive for this backup
confluence_restore_archive

# Restore the database
confluence_restore_db

# Restore the filesystem
confluence_restore_home

##########################################################
