#!/bin/bash

check_command "rsync"

function confluence_perform_rsync {
    RSYNC_QUIET=-q
    if [ "${CONFLUENCE_VERBOSE_BACKUP}" == "TRUE" ]; then
        RSYNC_QUIET=
    fi

    mkdir -p ${CONFLUENCE_BACKUP_HOME}
    rsync -avh ${RSYNC_QUIET} --delete --delete-excluded --exclude=/backups/	--exclude=/temp/ --exclude=/webresource-temp/ --exclude=/restore/ --exclude=/*.lock ${CONFLUENCE_HOME} ${CONFLUENCE_BACKUP_HOME}
    if [ $? != 0 ]; then
        bail "Unable to rsync from ${CONFLUENCE_HOME} to ${CONFLUENCE_BACKUP_HOME}"
    fi
}

function confluence_prepare_home {
    info "Prepared backup of ${CONFLUENCE_HOME} to ${CONFLUENCE_BACKUP_HOME}"
}

function confluence_backup_home {
    confluence_perform_rsync
    info "Performed backup of ${CONFLUENCE_HOME} to ${CONFLUENCE_BACKUP_HOME}"
}

function confluence_restore_home {
    RSYNC_QUIET=-q
    if [ "${CONFLUENCE_VERBOSE_BACKUP}" == "TRUE" ]; then
        RSYNC_QUIET=
    fi

    rsync -av ${RSYNC_QUIET} ${CONFLUENCE_RESTORE_HOME}/ ${CONFLUENCE_HOME}/
    info "Performed restore of ${CONFLUENCE_RESTORE_HOME} to ${CONFLUENCE_HOME}"
}
