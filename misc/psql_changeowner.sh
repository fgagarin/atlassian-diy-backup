#!/bin/bash

export PGPASSWORD=postgres 
export PGUSER=postgres
export PGDATABASE=jira
export OWNER=jira

for tbl in `psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -qAt -c "select tablename from pg_tables where schemaname = 'public';"` ; do  
	psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -c "alter table \"$tbl\" owner to $OWNER"; 
done

for tbl in `psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -qAt -c "select sequence_name from information_schema.sequences where sequence_schema = 'public';"`; do
	psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -c "alter table \"$tbl\" owner to $OWNER"; 
done

for tbl in `psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -qAt -c "select table_name from information_schema.views where table_schema = 'public';"`; do  
	psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -c "alter table \"$tbl\" owner to $OWNER";
done
