#!/bin/bash

##
# It is recomended to `chmod 600 stash.diy-backup.vars.sh` after copying the template.
##

CURL_OPTIONS="-L -s -f"

# Which database backup script to use (ex: mssql, postgresql, mysql, ebs-collocated, rds)
BACKUP_DATABASE_TYPE=postgresql

# Which filesystem backup script to use (ex: rsync, ebs-home)
BACKUP_HOME_TYPE=rsync

# Which archive backup script to use (ex: tar, tar-gpg)
BACKUP_ARCHIVE_TYPE=tar

# Used by the scripts for verbose logging. If not true only errors will be shown.
STASH_VERBOSE_BACKUP=TRUE

# The base url used to access this stash instance. It cannot end on a '/'
STASH_URL=localhost:7990

# Used in AWS backup / restore to tag snapshots. It cannot contain spaces and it must be under 100 characters long
INSTANCE_NAME=stash

# The username and password for the user used to make backups (and have this permission)
STASH_BACKUP_USER=admin
STASH_BACKUP_PASS=admin

# The name of the database used by this instance.
STASH_DB=stash
# The path to stash home folder (with trailing /)
STASH_HOME=/var/atlassian/stash/


# OS level user and group information (typically: 'atlstash' for both)
STASH_UID=daemon
STASH_GID=daemon

# The path to working folder for the backup
STASH_BACKUP_ROOT=/var/backups/stash
# DO NOT CHANGE FOLLOWING FOLDER!!! (they affect to restore script)
STASH_BACKUP_DB=${STASH_BACKUP_ROOT}/db/
STASH_BACKUP_HOME=${STASH_BACKUP_ROOT}/home/

# The path to where the backup archives are stored
STASH_BACKUP_ARCHIVE_ROOT=${STASH_BACKUP_ROOT}/backup

# List of repo IDs which should be excluded from the backup
# It should be space separated: (2 5 88)
STASH_BACKUP_EXCLUDE_REPOS=()

# PostgreSQL options
POSTGRES_HOST=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' postgres)
POSTGRES_USERNAME=stash
POSTGRES_PASSWORD=stash
POSTGRES_PORT=5432

# MySQL options
MYSQL_HOST=
MYSQL_USERNAME=
MYSQL_PASSWORD=
MYSQL_BACKUP_OPTIONS=

# HipChat options
HIPCHAT_URL=https://api.hipchat.com
HIPCHAT_ROOM=
HIPCHAT_TOKEN=

# Options for the tar-gpg archive type
STASH_BACKUP_GPG_RECIPIENT=
